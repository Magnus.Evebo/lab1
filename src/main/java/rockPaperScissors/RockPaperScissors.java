package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    //Random random = new Random(); 
    
    public void run() {
    	
    	while (true) {
			
    		//computerMove blir en random string fra rpsChoises lista
			String computerMove =  rpsChoices.get(new Random().nextInt(3));
			
				
			String round = "Let's play round "+roundCounter;
			System.out.println(round);
			
			String playerInput;
			while(true) {
				System.out.println("Your choice (Rock/Paper/Scissors)?");
				playerInput = sc.nextLine(); 
				if (playerInput.equals("rock") || playerInput.equals("paper") || playerInput.equals("scissors"))
					break;
				System.out.println("I do not understand "+playerInput+". Could you try again?");
			}
			
			String outcome = " ";
			
			if (playerInput.equals(computerMove)) {
				outcome = "It's a tie!";
			}
			else if (playerInput.equals("rock")) {
				if (computerMove.equals("paper")) {
					outcome = "Computer wins!";

					
				} else if (computerMove.equals("scissors")) {
					outcome = "Human wins!";
				}
			}
			else if (playerInput.equals("paper")) {
				if (computerMove.equals("rock")) {
					outcome = "Human wins!";
					
				} else if (computerMove.equals("scissors")) {
					outcome = "Computer wins!";
				}
			}
			else if (playerInput.equals("scissors")) {
				if (computerMove.equals("paper")) {
					outcome = "Human wins!";
					
				} else if (computerMove.equals("rock")) {
					outcome = "Computer wins!";
				}
			}
			
			
			//poengscore counter og round counter
			if (outcome.equals("It's a tie!")) {
				humanScore = humanScore+1;
				computerScore = computerScore+1;
			}
			else if (outcome.equals("Human wins!")) {
				humanScore = humanScore+1;
			}
			else {
				computerScore = computerScore+1;
			}
			
			roundCounter = roundCounter+1;
			
			System.out.println("Human chose "+playerInput+", computer chose "+computerMove+". "+outcome);
			System.out.println("Score: human "+humanScore+", computer "+computerScore);
			
	    	/* 	
	    	 * The code here does two things:
	    	 * It first creates a new RockPaperScissors -object with the
	    	 * code `new RockPaperScissors()`. Then it calls the `run()`
	    	 * method on the newly created object.
	         */
			System.out.println("Do you wish to continue playing? (y/n)?");
			String playAgain = sc.nextLine();
			if (playAgain.equals("y")) {
				run();
			}
			else {
				System.out.println("Bye bye :)");
			}
			break;
        
    	}
    	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
